# README #

This is a simple bash script that helps to fully backup your contacts from Google.
If you create a contact on your phone, and you add a picture, you'd like to have the picture kept with the contact's details if you do a backup.
When you export in vCard format, Google uses a web link to a picture stored in its system rather than embedding it.
If that link stops working for any reason (maybe if you delete the contact), that vCard won't show any picture.
Well, here the script that can fix it for you.

The script uses the vCard export from Google as input and process it, pulling the picture, encoding them and replacing the web link with this data.
You can now have your full contacts backup than you can safely store wherever you want :)

### How to use it ###

Simple run `google_contacts_photo_embedder.sh file.vcf`  
For extra help, you can also use `google_contacts_photo_embedder.sh -h`
