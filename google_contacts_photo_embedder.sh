#!/bin/bash

if [ $# -eq 0 ]
  then
    echo -e "No file provided.\nUse $0 myfile.vcf" ; exit 1
fi

#################################################################################################################
howto () {
	echo "
	This script is meant to process a Google Contacts vCard export ONLY.
	Google does NOT include the picture within the vcard, but instead, it returns
	a link to the picture.
	To have a full backup of the contact, including its picture, you have this script! :)

	- Just go to https://contacts.google.com/ 
	- Select the contacts you want to export
	- Choose Export -> Export as vCard
	- Save the file and give it to this script.

	Simply run $0 file.vcf

	The script will process the file and generate a new one, with the photo embedded in it.

	Happy backupping!

	"
}

fix_photo () {
	file=$1
	#backup
	cp $file $BAK_PATH/$file.bak

	grep ^CATEGORIES $file > $TMP_PATH/03_cat.txt
	echo 'END:VCARD' > $TMP_PATH/04_end.txt

	sed -n '/^PHOTO.*/q;p' $file > $TMP_PATH/01_head.txt

	URL=$(grep -A1 ^PHOTO $file | sed 's/^PHOTO:\(.*\)/\1/' | tr -d '\n' | tr -d ' ')
	wget -q $URL -O $TMP_PATH/photo.jpg
	sleep 1

	B64ENCPHOTO=$(base64 -w0 $TMP_PATH/photo.jpg)
	B64ENCPHOTO="PHOTO;ENCODING=BASE64;TYPE=JPEG:$B64ENCPHOTO"
	echo $B64ENCPHOTO | head -c 73 > $TMP_PATH/02_photo.txt
	for L in $(echo $B64ENCPHOTO | tail -c +74 | sed -e 's/.\{72\}/&\n/g'); do
	  echo -en "\n $L" >> $TMP_PATH/02_photo.txt
	done
	echo >> $TMP_PATH/02_photo.txt

	cat $TMP_PATH/01_head.txt $TMP_PATH/02_photo.txt $TMP_PATH/03_cat.txt $TMP_PATH/04_end.txt > $OUTPUT_PATH/new_$file
	rm -rf $TMP_PATH/*

	echo -e "Processed $OUTPUT_PATH/new_$file"
}


#################################################################################################################
# Get the options
while getopts ":h" option; do
   case $option in
      h) # display Help
         howto
         exit;;
   esac
done

vcffile=$1

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
WORKSPACE="$SCRIPT_DIR/workspace"
TMP_PATH="$WORKSPACE/tmp"
OUTPUT_PATH="$WORKSPACE/output"
BAK_PATH="$WORKSPACE/bak"
mkdir -p $WORKSPACE $TMP_PATH $OUTPUT_PATH $BAK_PATH

echo -e "\nProcessing $vcffile\n"

echo -e "Before continue, please make sure the file is in UNIX format.
Run dos2unix $vcffile if you are not sure.
Press any key to continue, or CTRL+C to Abort..."
read voidvar

echo "Split $vcffile - one vcf file per contact" 
awk ' /BEGIN:VCARD/ { close(fn); ++a; fn=sprintf("card_%02d.vcf", a); print "Writing: ", fn } { print $0 > fn; } ' $vcffile
mv card*.vcf $WORKSPACE/

cd $WORKSPACE
for vcf in $(ls card*.vcf); do
	if $(grep -q ^PHOTO $vcf) ; then 
		fix_photo $vcf
  	else 
		cp $vcf $OUTPUT_PATH/new_copy_$vcf
		echo -e "File $vcf does NOT contain any photo"
	fi
done

cd $OUTPUT_PATH
cat *.vcf > $SCRIPT_DIR/processed_$vcffile

echo -e "\n\nNew updated Full VCF file has been generated here: $SCRIPT_DIR/processed_$vcffile
You can safely delete $WORKSPACE now.\n\n"

#################################################################################################################
#################################################################################################################
#################################################################################################################
